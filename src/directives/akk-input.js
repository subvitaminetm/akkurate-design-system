/**
 * Akkurate v1.0.0 (https://ww.akkurate.io)
 * Copyright 2017-2018 Subvitamine(tm) (https://www.subvitamine.com)
 * Commercial License
 * @description: directive to create a zone for an text entry the entry can be use for ulterior fonction
 */

'use strict';

angular.module('akkurate-design-system')
    .directive('akkInput', [
            '$rootScope',
            function ($rootScope) {
                return {
                    templateUrl: 'templates/akk-input.html',
                    restrict: 'E',
                    transclude: true,
                    replace: true,
                    // eventUpdate: "@",
                    scope: {
                        label: "@",
                        elementclass: "@",
                        type: "@",
                        step: "@",
                        placeholder: "@",
                        req: "@",
                        disabled: "=",
                        hasError: "=",
                        model: "=",
                        eventUpdate: "@",
                        eventFocusIn: "@",
                        eventFocusOut: "@",
                    },
                    link: function postLink(scope, element, attrs, ngModel) {
                        scope.view = {
                            value: angular.copy(scope.model)
                        };

                        // Watch the 'hasError' attribute
                        scope.$watch('hasError', function () {
                            scope.isValid = (scope.hasError == null || scope.hasError != true);
                        });

                        scope.methods = {
                            // Check for validity after the element has lost focus
                            change: function () {
                                scope.isValid = element[0].children[1].validity.valid;

                                if (scope.eventUpdate && scope.model !== scope.view.value) {
                                    $rootScope.$broadcast(scope.eventUpdate, scope.model);
                                }
                            }
                        };
                    }
                }
            }
        ]
    );